#!/bin/bash
# Access env vars

# Specify the output file
output_file=".env"

# Create the environment file
echo "PGUSER=\"$PGUSER\"" > "$output_file"
echo "PGHOST=\"$PGHOST\"" >> "$output_file"
echo "PGPASSWORD=\"$PGPASSWORD\"" >> "$output_file"
echo "PGDATABASE=\"$PGDATABASE\"" >> "$output_file"
echo "PGPORT=\"$PGPORT\"" >> "$output_file"

# Display a message
echo "Environment variables written to $output_file"
# Start Docker service
sudo service docker start

# Build the Docker image
docker build -t novacomp_node .

# Authenticate to Amazon ECR
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 272924048139.dkr.ecr.us-east-1.amazonaws.com

# Tag the Docker image
docker tag novacomp_node:latest 272924048139.dkr.ecr.us-east-1.amazonaws.com/novacomp_node:latest

# Push the Docker image to Amazon ECR
docker push 272924048139.dkr.ecr.us-east-1.amazonaws.com/novacomp_node:latest

# Update the ECS service with a forced new deployment
# aws ecs update-service --force-new-deployment --service novacomp_service --cluster banistmo-v2
OLD_TASK_ID=$(aws ecs list-tasks --cluster banistmo-v2 --desired-status RUNNING --family novacomp-task-ecs | egrep "task/" | sed -E "s/.*task\/(.*)\"/\1/")
aws ecs stop-task --cluster banistmo-v2 --task $OLD_TASK_ID
## HINT: come back and add fixed values to env variables.