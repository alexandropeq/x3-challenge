#!/bin/bash

echo "$ENV_VARIABLE" > node_novacompa_keypair.pem

# Start Docker service
sudo service docker start

# Build the Docker image
docker build -t novacomp_node .

ssh -i node_novacompa_keypair.pem ec2-user@ec2-54-205-198-54.compute-1.amazonaws.com
