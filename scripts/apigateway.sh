#!/bin/bash

# Install Serverless globally
npm install -g serverless

# Install project dependencies
npm install

# Configure Serverless credentials
serverless config credentials --provider aws --key "$AWS_ACCESS_KEY_ID" --secret "$AWS_SECRET_ACCESS_KEY"

# Deploy the Serverless project
serverless deploy