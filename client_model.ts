export interface Client {
    full_name: string;
    income: number;
    city: string;
    date_of_birth: string;
    service: number;
}
export interface Service {
    id: number;
    service: string;
}