import { Client, Service } from "./client_model";
const dotenv = require("dotenv")
const { Pool } = require("pg");
dotenv.config();


const dbUser = {
    user: process.env.PGUSER,
    host: process.env.PGHOST,
    password: process.env.PGPASSWORD,
    port: process.env.PGPORT,
    ssl: {
        rejectUnauthorized: false,
    },
}

async function getQuery(query: string) {
    try{
        const poolDb = new Pool(dbUser);
        let data = await poolDb.query(query)
        await poolDb.end();
        return data.rows
    } catch (err){
        console.log("err", err)
        return err;
    }
}

async function postQuery(c: Client) {
    try{
        const poolDb = new Pool(dbUser);
        const insertQuery = `
            INSERT INTO clients (full_name, income, city, date_of_birth, service) 
            VALUES ($1, $2, $3, $4, $5);
        `;
        let data = await poolDb.query( insertQuery, [c.full_name, c.income, c.city, c.date_of_birth, c.service] );
        await poolDb.end();
        return Boolean(data.rowCount);
    } catch (err){
        console.log("err", err)
        return err;
    }
}

async function putQuery(id: number, c:Client) {
    try{
        const poolDb = new Pool(dbUser);
        const putQuery = `
            UPDATE clients 
            SET full_name = $1, income = $2, city = $3, date_of_birth = $4, service = $5
            WHERE id = $6
        `;
        let data = await poolDb.query( putQuery, [c.full_name, c.income, c.city, c.date_of_birth, c.service, id] );
        await poolDb.end();
        return Boolean(data.rowCount);
    } catch (err){
        console.log("err", err)
        return err;
    }
}

async function deleteQuery(id: number) {
    try{
        const poolDb = new Pool(dbUser);
        let data = await poolDb.query('DELETE FROM clients WHERE "id" = $1', [id]);
        await poolDb.end();
        return Boolean(data.rowCount);
    } catch (err){
        console.log("err", err)
        return err;
    }
}

async function getServicesPerUser(id: number) {
    try{
        const poolDb = new Pool(dbUser);
        let data = await poolDb.query(`SELECT * FROM  clients WHERE id = ${id}`)
        let options = await poolDb.query(`SELECT * FROM  services`)
        let services: string[] = [];
        const {full_name, income, city, date_of_birth} = data.rows[0]

        options.rows.forEach((o: Service) => {
            if(calculateAge(date_of_birth) > 18){
                if(o.id == 1){
                    services.push(o.service)
                }
            }
            if(calculateAge(date_of_birth) > 18 && income > 500){
                if(o.id == 2){
                    services.push(o.service)
                }
            }
            if(calculateAge(date_of_birth) > 18 && income > 1500){
                if(o.id == 3){
                    services.push(o.service)
                }
            } 
            if(calculateAge(date_of_birth) > 15 && income > 1000){
                if(o.id == 4){
                    services.push(o.service)
                }
            }
            if(calculateAge(date_of_birth) > 25 && income > 4500){
                if(o.id == 5){
                    services.push(o.service)
                }
            }
            if(city.toLowerCase() === "panama"){
                if(o.id == 6){
                    services.push(o.service)
                }
            }
        });

        await poolDb.end();
        return {
            client: full_name,
            available_options: services
        };
    } catch (err){
        console.log("err", err)
        return err;
    }
}

function calculateAge(d: Date): number {
    var dob = new Date(d);  
    var month_diff = Date.now() - dob.getTime();  
    var age_dt = new Date(month_diff);   
    var year = age_dt.getUTCFullYear();  
    var age = Math.abs(year - 1970); 
    return age;
}


module.exports = {getQuery, postQuery, putQuery, deleteQuery, getServicesPerUser};
