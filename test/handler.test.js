var db = require('../db');

test("Get services list", async () => {
    const services_list = await db.getQuery('SELECT * FROM services')    
    services_list.forEach(e => {
        expect(e).toHaveProperty('id')
        expect(e).toHaveProperty('service')
    });
});


test("Get users list", async () => {
    const users_list = await db.getQuery('SELECT * FROM clients')    
    users_list.forEach(e => {
        expect(e).toHaveProperty('id')
        expect(e).toHaveProperty('full_name')
        expect(e).toHaveProperty('income')
        expect(e).toHaveProperty('city')
        expect(e).toHaveProperty('date_of_birth')
    });
});

test("Add user", async () => {

    client = {
		"full_name": "Generic User",
		"income": 999.45,
		"city": "Mexicali",
		"date_of_birth": "09/07/1987",
		"service": 0        
    }

    expect(client).toHaveProperty('full_name')
    expect(client).toHaveProperty('income')
    expect(client).toHaveProperty('city')
    expect(client).toHaveProperty('date_of_birth')

    const user_added = await db.postQuery(client);

    expect(user_added).toBe(true);
});


// test("Delete user", async () => {

//     id = 21;

//     expect(client).toHaveProperty('full_name')
//     expect(client).toHaveProperty('income')
//     expect(client).toHaveProperty('city')
//     expect(client).toHaveProperty('date_of_birth')

//     const delete_user = await db.deleteQuery(id);

//     expect(delete_user).toBe(true);
// });