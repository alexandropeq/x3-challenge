const serverless = require("serverless-http");
const express = require("express");
const app = express();
const port = 3000;
const db = require('./db');

app.use(express.json());

app.get("/users/:id", async (req: any, res: any) => {
    try {
        let data = await db.getQuery(`SELECT * FROM  clients WHERE id = ${req.params.id}`)
        if(data.length <= 0)
            return res.status(404).json({
                message: "Seems like client does not exist, please try again...",
            });
        return res.status(200).json({data});
    } catch(err) {
        return res.status(400).json({
            err,
            message: "Unable to get client, please try again...",
        });
    }
});

app.post("/users", async(req: any, res: any) => {
    try {
        let data = await db.postQuery(req.body)
        return res.status(200).json({data});
    } catch(err) {
        return res.status(404).json({
            err,
            message: "Unable to add new client, please try again...",
        });
    }
});

app.put("/users/:id", async (req: any, res: any) => {
    try {
        let data = await db.putQuery(req.body.id, req.body.client)
        return res.status(200).json({data});
    } catch(err) {
        return res.status(404).json({
            err,
            message: "Unable to update client, please try again...",
        });
    }
});

app.delete("/users/:id", async (req: any, res: any) => {
    try {
        let data = await db.deleteQuery(req.params.id)
        return res.status(200).json({data});
    } catch(err) {
        return res.status(404).json({
            err,
            message: "Unable to delete client, please try again...",
        });
    }
});

app.get("/services", async (req: any, res: any) => {
    try {
        let data = await db.getQuery('SELECT * FROM services')
        return res.status(200).json({data});
    } catch(err) {
      return res.status(404).json({
          err,
          message: "Unable to retrieve list of services, please try again...",
      });
    }
});
  
app.get("/services/:id", async (req: any, res: any) => {
    try {
        let data = await db.getServicesPerUser(req.params.id)
        return res.status(200).json({data});
    } catch(err) {
    return res.status(404).json({
        err,
        message: "Unable to retrieve list of services, please try again...",
    });
    }
});

app.get('/status', (req: any, res: any) => res.send({status: "I'm up and running from ECS V.2"}));

app.use((req: any, res: any) => {
    return res.status(404).json({
      error: "Not Found",
    });
});


// Debug purposes don't uncomment when deploying to serverless
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
});

module.exports.handler = serverless(app);
